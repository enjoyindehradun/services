var assert = require('assert');
var replit = require ('../..').replit;
var parse = replit.parse;
var examples = replit.examples;

describe('repl.it', function () {
  it('with one submission generate a summary notification', function () {
    var payload = examples['submit'];

    var response = parse(payload.headers, payload.body);

    assert.equal(response.message, 'John Doe submitted **complete** for 1.3. Input/print: Hello, Harry!  \n\
Click [here](https://repl.it/teacher/submissions/7399304) to review this submission.');
    assert.equal(response.icon, 'logo');
    assert.equal(response.errorLevel, 'normal');
  });

  /*
  it('with no submission should not generate a notification', function () {
    var payload = examples['submit_none'];

    var response = parse(payload.headers, payload.body);

    assert(!response);
  });
  */
});
